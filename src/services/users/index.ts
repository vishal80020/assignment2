/**
 * Service class and methods for dummy APIs
 */

import { User } from "../../models/user";

import { Address } from "../../models/address";

import { Book } from "../../models/book";

 
 
class UsersService {

    /**
     * Method to get list of dummies
     * @returns {Promise<string[]>}
     */
    public static async get(req:any): Promise<any>  {
        //NOTE: Add validators and types as required.
        //      Refer playbooks API service for examples

     
        const userData = await User.findOne({"email": req.params.emailid}).lean();
        console.log(userData);
        const name:String = userData.firstname +" "+userData.lastname;
        const email :String  = userData.email;
        const phone : String = userData.phone;
        
        const addressId:String = userData.address
        const bookId = userData.rentedBooks;


        console.log(name)

        console.log(bookId);
           
        // console.log(email.length());
        // const usern = email.substr(0,email.length() -7);
        // // console.log(usern);
        // const userNumber =usern.substr(usern.length()-1);
        // const addressId = "adr"+userNumber;

        

        // console.log(bookId)

        const addressData = await Address.findOne({"addressId": addressId}).lean();
        console.log(addressData);
        const address : String = addressData.house+","+" "+addressData.street +","+" "+addressData.city+" "+"-"+" "+addressData.postalCode;
        const country =addressData.country;

        console.log(address);

        var rentedBooks=[];

        for(var i=0;i<bookId.length;i++){
            var bookData = await Book.find({"bookId": bookId[i]}).lean();
            rentedBooks[i]={
                "isbn": bookData[0].isbn,
                "title": bookData[0].title,
                "subtitle": bookData[0].subtitle,
                "author": bookData[0].author
            };
        }


        const userDetails =[{
            "name":name,
            "email": email,
            "phone": phone,
            "address": address,
            "country": country,
            "rentedBooks": rentedBooks
        }];
        console.log(userDetails);

        return userDetails[0];

        
      }

    }

export { UsersService };