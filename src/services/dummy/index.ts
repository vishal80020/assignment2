/**
 * Service class and methods for dummy APIs
 */

class DummyService {

    /**
     * Method to get list of dummies
     * @returns {Promise<string[]>}
     */
    public static async get(): Promise<string[]> {
        //NOTE: Add validators and types as required.
        //      Refer playbooks API service for examples
        return [];
    }
}

export { DummyService };