/**
 * Controller for all dummy APIs
 */

import { Router, Request, Response, NextFunction } from 'express';

import { UsersService } from './../../../services/users';

const router: Router = Router();

/**
 * Get dummy list
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
router.get('/:emailid', async (req: Request, res: Response, next: NextFunction) => {
    const { emailId } = req.params;
    try {
        res.json(await UsersService.get(req));
    } catch (err) {
        next(err);
    }
});

export default router;