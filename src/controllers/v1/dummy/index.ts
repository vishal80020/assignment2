/**
 * Controller for all dummy APIs
 */

import { Router, Request, Response, NextFunction } from 'express';

import { DummyService } from './../../../services/dummy';

const router: Router = Router();

/**
 * Get dummy list
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
router.get('/', async (req: Request, res: Response, next: NextFunction) => {
    const { cloudType } = req.params;
    try {
        res.json(await DummyService.get());
    } catch (err) {
        next(err);
    }
});

export default router;